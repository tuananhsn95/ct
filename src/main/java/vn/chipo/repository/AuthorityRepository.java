package vn.chipo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.chipo.domain.Authority;

/**
 * Spring Data MongoDB repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends MongoRepository<Authority, String> {}
