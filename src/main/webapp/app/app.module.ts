import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ChipoToolsAngularSharedModule } from 'app/shared/shared.module';
import { ChipoToolsAngularCoreModule } from 'app/core/core.module';
import { ChipoToolsAngularAppRoutingModule } from './app-routing.module';
import { ChipoToolsAngularHomeModule } from './home/home.module';
import { ChipoToolsAngularEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    ChipoToolsAngularSharedModule,
    ChipoToolsAngularCoreModule,
    ChipoToolsAngularHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ChipoToolsAngularEntityModule,
    ChipoToolsAngularAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class ChipoToolsAngularAppModule {}
